from django.shortcuts import render, get_object_or_404
from . import models


def tv_show_view(request):
    tv_show = models.TvShow.objects.all()
    return render(request, 'tv_show_html/tv_show.html', {'tv_show': tv_show})


def tv_show_detail_view(request, id):
    tv_show_id = get_object_or_404(models.TvShow, id=id)
    return render(request, 'tv_show_html/tv_show_detail.html',
                  {'tv_show_id': tv_show_id})
